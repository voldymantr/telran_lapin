package filereader;

import java.io.*;

public class RWFiles {
    public static String fileReader(String fileName){
        String text =  "";
        try {
            File file = new File(fileName);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line = "";
            while(line != null){
                line = br.readLine();
                text += line + " ";
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text;
    }

    public static void appendToFile(String text, String fileName){
        char a = 10;
             text = text.replace("null ", String.valueOf(a));
        try {
            File file = new File(fileName);
            if (!file.exists()) {
                file.createNewFile();
            }
            BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));
            bw.write(text);
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
