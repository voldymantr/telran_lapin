package firstProject;

import java.text.ParseException;


public class Main {

    public static void main(String[] args) throws ParseException {
       System.out.println("Starting ...");
       System.out.println("launch method reversString() ...");
       System.out.println(ReversString.reversString("Abracadabra"));
       System.out.println("launch method reversString1() ...");
       System.out.println(ReversString.reversString1("Abracadabra"));
       System.out.println("launch method isPalindrome() ...");
       System.out.println(Palindrome.isPalindrome("Are we not drawn onward, we few, drawn onward to new era?"));
       System.out.println("launch method formatDate() ...");
       System.out.println(DateTool.formatDate("15-09-2009", "dd-mm-yyy", "dd/mm/yyy"));
    }
}
