package firstProject;

public class ReversString {
    public static String reversString(String stringToRevers){
        char[] str = stringToRevers.toCharArray();
        char c = ' ';
        for ( int i=0, j = stringToRevers.length()-1;  (i < (stringToRevers.length()/2)) & (j > (stringToRevers.length()/2));  i++,j--){
            c = str[i]; str[i]=str[j]; str[j]=c;
        }
        return String.copyValueOf(str)  ;
    }

    public static String reversString1(String stringToRevers){
        StringBuilder sb = new StringBuilder(stringToRevers);
        return sb.reverse().toString();
    }
   }

