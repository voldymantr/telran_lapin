package firstProject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTool {
    public static String formatDate(String dateToFormat, String originalFormat, String desiredFormat) throws ParseException {
        Date date = new SimpleDateFormat(originalFormat).parse(dateToFormat);
        return new SimpleDateFormat(desiredFormat).format(date);
    }
}
